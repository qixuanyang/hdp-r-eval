
load_data <- function(path){
  require(readr)
  try(detach("package:igraph", unload = T), silent = T)
  
  dat <- read_delim(path, delim = " ")
  names(dat) <- c("doc", "word", "topic", "table")
  
  dat$word <- dat$word + 1
  dat$topic <- dat$topic + 1
  dat$table <- dat$table + 1
  
  return(dat)
}

print_topics <- function(words_fn, vocab_fn, topics_fn, top_n = 5, verbose = F){
  
  require(plyr)
  require(dplyr)
  require(readr)
  try(detach("package:igraph", unload = T), silent = T)
  
  word_topic <- as.matrix(read_table(words_fn, col_names = FALSE, cols(.default = col_integer())))
  vocab <- read_lines(vocab_fn)
  word_topic <- t(word_topic)
  
  word_topic_prob <- sapply(1:ncol(word_topic), function(x) word_topic[,x]/colSums(word_topic)[x])
  word_topic_prob <- as_data_frame(word_topic_prob)
  word_topic_prob_zero <- sapply(1:ncol(word_topic_prob), function(x) which(unlist(word_topic_prob[,x]) == 0))
                                    
  word_topic_out <- sapply(1:ncol(word_topic_prob), function(x) sort.int(x = unlist(word_topic_prob[,x]), decreasing=TRUE, index.return=TRUE)$ix)
  word_topic_out <- plyr::mapvalues(word_topic_out, 1:length(vocab), vocab)
  word_topic_out <- as_data_frame(word_topic_out)
  names(word_topic_out) <- paste0("tpc",c(1:ncol(word_topic_out)))
  
  write_csv(head(word_topic_out, top_n), topics_fn)
  
  if(verbose == T){
    print(head(word_topic_out, top_n))
  }
  
  return(head(word_topic_out, top_n))
}

print_tables <- function(data, vocab, topic_num, top_n = 5, verbose = F){
  
  require(plyr)
  require(dplyr)
  try(detach("package:igraph", unload = T), silent = T)
  
  data <- dat
  table_mat <- spread(dplyr::count(filter(data, topic == topic_num), word, table), table, n, fill = 0)
  
  table_mat_red <- table_mat[,-1]
  table_mat_red_prob <- sapply(1:ncol(table_mat_red), function(x) table_mat_red[,x]/colSums(table_mat_red)[x])
  table_mat_red_prob <- as_data_frame(table_mat_red_prob)
  
  table_mat_red_prob_zero <- sapply(1:ncol(table_mat_red_prob), function(x) which(unlist(table_mat_red_prob[,x]) == 0))
  
  table_mat_red_out <- sapply(1:ncol(table_mat_red_prob), function(x) sort.int(x = unlist(table_mat_red_prob[,x]), decreasing=TRUE, index.return=TRUE)$ix) 
  
  for(col in 1:ncol(table_mat_red_out)){
    table_mat_red_out[,col] <- ifelse(table_mat_red_out[,col] %in% as.numeric(table_mat_red_prob_zero[[col]]), NA, table_mat_red_out[,col])
  }
  
  table_mat_red_out <- plyr::mapvalues(table_mat_red_out, 1:nrow(table_mat_red_out), unlist(table_mat[,1]))
  
  table_mat_red_out <- plyr::mapvalues(table_mat_red_out, 1:length(vocab), vocab, warn_missing = F)
  
  table_mat_red_out <- as_data_frame(table_mat_red_out)
  names(table_mat_red_out) <- names(table_mat_red_prob)
  
  names(table_mat_red_out) <- paste0("tpc",topic_num, "_tbl", names(table_mat_red_out))
  
  if(verbose == T){
    print(head(table_mat_red_out, top_n))
  }
  
  out <- table_mat_red_out
  
  return(out)
}

bind_all_tbl <- function(data, vocab, top_n = 5, n_topics){
  all_tables <- sapply(1:n_topics,
                       function(x) t(print_tables(data = dat, vocab = vocab, topic_num = x, top_n = top_n, verbose = F)) %>% as_data_frame) %>% bind_rows()
  
  all_tables <- as_data_frame(t(all_tables))
  
  names(all_tables) <- lapply(1:n_topics, function(x) print_tables(data = dat, vocab = vocab, topic_num = x, top_n = top_n) %>% names) %>% unlist()
  
  return(all_tables)
}

aggr_tpc_tbl <- function(dat){
  
  require(dplyr)
  try(detach("package:igraph", unload = T), silent = T)
  
  dat_red <- dat %>% group_by(topic, table) %>% dplyr::summarise(n=n())
  dat_red$topic <- paste0("tpc",dat_red$topic)
  dat_red$table <- paste0(dat_red$topic, "_", paste0("tbl",dat_red$table))
  dat_red <- dat_red %>% group_by(topic) %>% dplyr::mutate(weight_cond_tpc = n/sum(n))
  return(dat_red)
}

networked_data <- function(topics, all_tables, dat_red){
  require(igraph)
  require(RColorBrewer)
  require(stringr)
  require(dplyr)
  
  num_nodes <- ncol(topics) + ncol(all_tables)
  net_mat <- matrix(0, nrow = num_nodes, ncol = num_nodes)
  
  row.names(net_mat) <- c(names(topics), names(all_tables))
  colnames(net_mat) <- c(names(topics), names(all_tables))
  
  for(i in 1:nrow(net_mat)){
    for(j in 1:ncol(net_mat)){
      if(str_match(row.names(net_mat)[i], "tpc\\d{1,2}") == str_match(colnames(net_mat)[j], "tpc\\d{1,2}")){
        if(!(str_detect(row.names(net_mat)[i], "tpc\\d{1,2}[_]") & str_detect(colnames(net_mat)[j], "tpc\\d{1,2}[_]"))){
          net_mat[i,j] = 1
        }
      }
    }
  }
  
  
  diag(net_mat) <- 0  # Eliminate self-referring
  
  net <- graph_from_adjacency_matrix(net_mat, mode = "upper")
  net <- simplify(net, remove.multiple = F, remove.loops = T) 
  
  V(net)$type <- c(rep(F, ncol(topics)), rep(T, ncol(all_tables)))
  
  V(net)$color <- ifelse(V(net)$type == F, "steel blue", "orange")
  V(net)$shape <- ifelse(V(net)$type == T, "square", "circle")
  
  V(net)$label <- ""
  V(net)$label[V(net)$type == F] <- sapply(1:ncol(topics), function(x) str_c(unlist(na.omit(head(topics[,x]))), collapse = " "))
  V(net)$label[V(net)$type == T] <- sapply(1:ncol(all_tables), function(x) str_c(unlist(na.omit(head(all_tables[,x]))), collapse = " "))
  
  pal <- brewer.pal(11, "Spectral")
  pal = colorRampPalette(pal)(ncol(topics))
  ecols <- rep(NA, length(E(net)))
  
  for(i in 1:ncol(topics)){
    temp <- str_detect(attr(E(net), "vnames"), paste0("tpc",i,"[|]"))
    ecols <- ifelse(temp == T, pal[i], ecols)
  }
  
  E(net)$color <- ecols
  
  edge_dat <- data_frame(id = attr(E(net),"vnames"))
  dat_red$id <- paste0(dat_red$topic, "|",dat_red$table)
  edge_dat <- left_join(edge_dat, dat_red)
  
  E(net)$weight <- edge_dat$weight_cond_tpc
  
  return(net)
}

read_state <- function(path){
  require(readr)
  require(dplyr)
  try(detach("package:igraph", unload = T), silent = T)
  
  state <- read_lines(path)
  state <- str_trim(state)
  state <- lapply(1:length(state), function(x) unlist(str_split(state[x], " "))) %>% bind_cols()
  state <- t(state)
  colnames(state) <- state[1,]
  state <- as_data_frame(state[-1,])
  state$time_lag <- c(0, diff(as.numeric(state$time),1))
  
  return(state)
}

plot_state_info <- function(dat, metric){
  y <- as.numeric(unlist(dat[,metric]))
  x <- 1:length(y)
  plot(x, y, type = "l", ylab = metric, xlab = "Iteration")
  lines(loess.smooth(x,y, span = 1/5), col = "red", lwd = 2)
}

# Posterior evaluation code for Hierarchical Dirichlet Process in R

The code here provides a some useful functions to evaluate and visualize the posterior estimates from the Hierarchical Dirichlet Process (HDP), a multi-layered unsupervised topic models proposed by [Teh, Jordan, Beal, and Blei (2006)](https://amstat.tandfonline.com/doi/abs/10.1198/016214506000000302). This repository does not include the training code in C++ for HDP developed by Chong Wang and David Blei (2010, [Github repository click here](https://github.com/blei-lab/hdp)). Nevertheless, this file shall begin with some initial notes for the training to help users understand the parameters.

In this repository, I provide an example that is easy to replicate in terms of data gathering and analysis: __The speeches of the German Chancellor Angela Merkel from 2014 to 2017__, scraped automatically from [the official website of the Federal Chancellory](www.bundeskanzlerin.de). Due to the copyright concerns, I do not post the whole texts here, but the tokenized version in LDA-C format (explanation see below). The web-scraping was mostly conducted via `rvest`-package in R and can be sent upon request. Further, the analysis is seeded, which facilitates the replication as well.

## Notes on training

Users need to closely follow the instruction depicted in the README on [blei-lab/hdp]((https://github.com/blei-lab/hdp)) to train the data after the entire repository is cloned. An example code please see [hdp-run-in-terminal.txt](https://gitlab.com/qxyang/hdp-r-eval/blob/master/hdp-run-in-terminal.txt). For a better workflow, there are two suggestions:

- Please make sure that the directories specified are right, especially for the directories where the posteriors will be saved. Use `mkdir [posterior-dir-name]` to create the folder.
- Please set the directories of training files as a parallel folder to your cloned hdp directory, especially when you have different datasets to train on with the same algorithm.

For the setup, please clone this repository and [blei-lab/hdp]((https://github.com/blei-lab/hdp)) in the same folder.

## Generating compatible data formats for the training

Additional note on the required lda-c format: Users can use the example codes I provide in [corpora_creation.r](https://gitlab.com/qxyang/hdp-r-eval/blob/master/corpora_creation.r) to easily export the existing corpora/term-document-matrix in the required format (DO NOT include the brackets - My initial mistake when running HDP for the first time...):

    [M] [term_1]:[count] [term_2]:[count] ...  [term_N]:[count]

This can be easily done through such a workload: creating text corpora (e.g. using `tm::Corpus`) -> tidying the texts (using `quanteda::dfm()`, for instance) -> gather the document-term-matrix (A great function from `tidytext::tidy()` solves this problem quite efficiently) -> use `base::table()` iteratively for each document to extract the information and format it in the above mentioned way, or apply a nice shortcut via `stm::writeLdac()` to export the LDA-C file (`train.dat` in this example). 

Please keep in mind that the terms are *numerically* represented - This is why you need `plyr::mapvalues` to replace the substantive tokens with numbers, and export a `vocab.dat` with substantive words that entirely matches your representation.

The main metrics being evaluating are three: `mode-topics.dat`, `mode-word-assignments.dat`, and `state.log`. We will take a look at each of them closely in the following section.

## Evaluation

All functions are saved in [lookup_utils.r](https://gitlab.com/qxyang/hdp-r-eval/blob/master/lookup_utils.r). We will go through all important functions and the philosophy behind them.

### print_topics()

Although [blei-lab/hdp]((https://github.com/blei-lab/hdp)) provides a script to plot the topics, its iterative style might subject to low efficiency. Therefore, I adapt their codes using a `readr`, `base::sapply`, `dplyr`, and `plyr`. In general, I exploit `readr::read_table()` to speed up the input of the word-topic matrix (named as `[posterior-repo]/mode-topics.dat`), employ the apply-method to realize divide-and-conquer style (although there exist better packages to realize parallelization, experienced users may replace the current version), and map the words into their numerical representation through `plyr::mapvalues`. 

The output consists of two components: A word-topic assignments based on their frequencies saved as a desired R-object, and a `.dat` file that consists of n-top words for each topic saved to a user-defined repository.

*Unresolved issues*: Extending the top-words definition beyond the pure occurence - i.e., incorporating the other algorithms (like lift and frex) into the top-word generation; Excluding words with zero-occurences (Although it might be a minor issue for topics for they are clustered in a fairly coarsed way, but still needed for the algorithmic correctness!)

### print_tables()

I apply the same rationale behind `print_topics()` to generate a word-table matrix.
















library(tidyverse)
library(quanteda)
library(readxl)
library(ggthemes)
library(tm)
library(topicmodels)
library(stm)
library(lda)
library(SnowballC)
library(jiebaR)
library(stopwords)
library(plyr)

# Load data ---------------------------------------------------------------

merkel <- read_csv2("merkelstatement_clean_with_content.csv")
names(merkel) 
  #' This is a web-scraped data (Qixuan Yang, 2018/2019). The full content is listed in content

merkel[67, c("content")] # Example

  #' Since it is Chinese language, it is required that the words shall be splitted. Here, we use
  #' jiebaR to split the words. The reason for using this package instead of a whole-sale solution
  #' provided by many corpus-generating packages is that we are better to tune what kind of mode of split,
  #' what kind of stopwords we may exclude etc.


# Split characters into dictionary mode -----------------------------------

dfm_merkel <- dfm(merkel$content, tolower = T, remove = stopwords("de"),
                  remove_symbols = T, remove_punct = T, remove_separators = T,
                  remove_numbers = T)
dfm_merkel <- dfm_wordstem(dfm_merkel, language = "german")
dfm_merkel

library(tidytext)

td_merkel <- tidy(dfm_merkel)
td_merkel$term_num <- td_merkel$term

dict <- unique(td_merkel$term)

td_merkel$term_num <- plyr::mapvalues(td_merkel$term_num, dict, 1:length(dict))


# Using stm ---------------------------------------------------------------


doc_list <- function(doc){
  temp <- filter(td_merkel, document == doc) %>% select(term_num, count)
  temp <- sapply(temp, as.numeric) %>% t()
}

doc_list_num <- lapply(unique(td_merkel$document), doc_list)

writeLdac(doc_list_num, "text/train.dat")
write(str_c(dict, collapse = "\n"), "text/vocab.dat")










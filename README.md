# Posterior evaluation code for Hierarchical Dirichlet Process in R

*Qixuan Yang. Update: 11th of February, 2019*

The code here provides a some useful functions to evaluate and visualize the posterior estimates from the Hierarchical Dirichlet Process (HDP), a multi-layered unsupervised topic models proposed by [Teh, Jordan, Beal, and Blei (2006)](https://amstat.tandfonline.com/doi/abs/10.1198/016214506000000302). This repository does not include the training code in C++ for HDP developed by Chong Wang and David Blei (2010, [Github repository click here](https://github.com/blei-lab/hdp)). Nevertheless, this file shall begin with some initial notes for the training to help users understand the parameters.

In this repository, I provide an example that is easy to replicate in terms of data gathering and analysis: __The speeches of the German Chancellor Angela Merkel from 2014 to 2017__, scraped automatically from [the official website of the Federal Chancellory](https://www.bundeskanzlerin.de/bkin-de). Due to the copyright concerns, I do not post the whole texts here, but the tokenized version in LDA-C format (explanation see below). The web-scraping was mostly conducted via `rvest`-package in R and can be sent upon request. Further, the analysis is seeded, which facilitates the replication as well.

## Table of contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Notes on training](#notes-on-training)
- [Generating compatible data formats for the training](#generating-compatible-data-formats-for-the-training)
- [Evaluation](#evaluation)
  - [Getting a word-topic matrix: `print_topics()`](#getting-a-word-topic-matrix-print_topics)
  - [Investigating word-table matrices: `print_tables()`](#investigating-word-table-matrices-print_tables)
  - [Understanding the hierarchical structure through visualization: `networked_data()`](#understanding-the-hierarchical-structure-through-visualization-networked_data)
  - [Checking the training state for each iteration: `plot_state_info()`](#checking-the-training-state-for-each-iteration-plot_state_info)
- [Future work](#future-work)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Notes on training

Users need to closely follow the instruction depicted in the README on [github::blei-lab/hdp](https://github.com/blei-lab/hdp) to train the data after the entire repository is cloned. An example code please see [hdp-run-in-terminal.txt](https://gitlab.com/qxyang/hdp-r-eval/blob/master/hdp-run-in-terminal.txt). For a better workflow, there are two suggestions:

- Please make sure that the directories specified are right, especially for the directories where the posteriors will be saved. Use `mkdir [posterior-dir-name]` to create the folder.
- Please set the directories of training files as a parallel folder to your cloned hdp directory, especially when you have different datasets to train on with the same algorithm.

For the setup, please clone this repository and [github::blei-lab/hdp](https://github.com/blei-lab/hdp) in the same folder.

## Generating compatible data formats for the training

Additional note on the required lda-c format: Users can use the example codes I provide in [corpora_creation.r](https://gitlab.com/qxyang/hdp-r-eval/blob/master/corpora_creation.r) to easily export the existing corpora/term-document-matrix in the required format (DO NOT include the brackets - My initial mistake when running HDP for the first time...):

    [M] [term_1]:[count] [term_2]:[count] ...  [term_N]:[count]

This can be easily done through such a workload: creating text corpora (e.g. using `tm::Corpus`) -> tidying the texts (using `quanteda::dfm()`, for instance) -> gather the document-term-matrix (A great function from `tidytext::tidy()` solves this problem quite efficiently) -> use `base::table()` iteratively for each document to extract the information and format it in the above mentioned way, or apply a nice shortcut via `stm::writeLdac()` to export the LDA-C file (`train.dat` in this example). 

Please keep in mind that the terms are *numerically* represented - This is why you need `plyr::mapvalues` to replace the substantive tokens with numbers, and export a `vocab.dat` with substantive words that entirely matches your representation.

The main metrics being evaluating are three: `mode-topics.dat`, `mode-word-assignments.dat`, and `state.log`. We will take a look at each of them closely in the following section.

## Evaluation

All functions are saved in [lookup_utils.r](https://gitlab.com/qxyang/hdp-r-eval/blob/master/lookup_utils.r). We will go through all important functions and the philosophy behind them.

### Getting a word-topic matrix: `print_topics()`

Although [github::blei-lab/hdp](https://github.com/blei-lab/hdp) provides a script to plot the topics, its iterative style might subject to low efficiency. Therefore, I adapt their codes using a `readr`, `base::sapply`, `dplyr`, and `plyr`. In general, I exploit `readr::read_table()` to speed up the input of the word-topic matrix (named as `[posterior-repo]/mode-topics.dat`), employ the apply-method to realize divide-and-conquer style (although there exist better packages to realize parallelization, experienced users may replace the current version), and map the words into their numerical representation through `plyr::mapvalues`. 

The output consists of two components: A word-topic assignments based on their frequencies saved as a desired R-object, and a `.dat` file that consists of n-top words for each topic saved to a user-defined repository.

*Issues*: 

- [ ] Extending the top-words definition beyond the pure occurence - i.e., incorporating the other algorithms (like lift and frex) into the top-word generation
- [x] Excluding words with zero-occurences (Although it might be a minor issue for topics for they are clustered in a fairly coarsed way, but still needed for the algorithmic correctness!)

### Investigating word-table matrices: `print_tables()`

I employ the same rationale behind `print_topics()` to generate a word-table matrix. The design of the HDP does allow the spill-over of words across the clusters (topics) and sub-clusters (tables). However, as [Paisley, Wang, Blei, and Jordan (2014)](https://arxiv.org/pdf/1210.6738.pdf) visually pointed out (page 2), there exists a strong single-membership training procedure and posterior distribution form. Therefore, the function provides the word representation of the tables *given* a topic. 

After iteratively/seperately finding the table representation for each topic, `bind_all_tbl()` bind all of the results together.

*Issues*:

- [ ] Extending the top-words definition beyond the pure occurence - i.e., incorporating the other algorithms (like lift and frex) into the top-word generation
- [ ] Considering an alternative way to represent the tables - Maybe penalizing the words that are already exists with a high probability in the topic? (Intuition: Tables shall be representing different facets of a topic)

### Understanding the hierarchical structure through visualization: `networked_data()`

To visualize the table-topic relation, `networked_data()` uses `package::igraph` to generate a matrix that denotes the relationships between topics and tables. After the production of the matrix, I utilize the share of documents classified to a table-topic relation given a topic to denote the weights of their relationships (`aggr_tpc_tbl()`). Formally speaking, for a table $`tbl_{i}`$ and $`tpc_{j}`$, and for each document $`d \in \{1,2,\dots,\Vert D\Vert \}`$:

```math
w(tbl_i | tpc_j) = \frac{\sum_{d=1}^{\Vert D \Vert} \mathbf{1}(TABLE(d) = tbl_i | tpc_j)}{\sum_{d=1}^{\Vert D \Vert} \mathbf{1}(TOPIC(d) = tpc_j)}
```
Where $`TABLE()`$ is the mapping from a document to a table, and $`TOPIC()`$ translates a document to a topic. Please refer to [lookup.ipynb](https://gitlab.com/qxyang/hdp-r-eval/blob/master/lookup.ipynb) to see the workflow of plotting this networked data.

*Issues*:
- [ ] Not only looking at the final classification but the probabilistic output of the posterior. Will the aggregation give another result?
- [ ] Incorporating the length of documents in the function? 

### Checking the training state for each iteration: `plot_state_info()`

After using `read_state()` to import the status log provided by HDP, one can easily visualize the metrics against the iterations by calling `plot_state_info`, whether the raw line and a loess-line are plotted.

## Future work
- [ ] Rewriting the functions into a package
- [ ] Resolving the issues raised above
- [ ] Creating a work-flow to systematically incorporating human intervention (semantic checks, and reimport)















